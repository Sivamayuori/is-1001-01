#include <stdio.h>
//This function swaps the values pointed by ap and bp
void swap (int *ap, int *bp){
int temp = *ap;
*ap=*bp;
*bp=temp;
}

int main(){
int a, b;

//Entering the value of a
puts ("Please enter the value of a: ");

//Reading the value of a
scanf ("%d", &a);

//Entering the value of b
puts ("Please enter the value of b: ");

//Reading the value of b
scanf ("%d", &b);

swap (&a, &b);

//Printing the swapped values
printf ("The answer after swapping is: a=%d, b=%d", a, b);

return 0;
}

