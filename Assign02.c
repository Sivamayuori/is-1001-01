#include <stdio.h>
int main(){
int radius;
float Pi_Value=3.14, area;

//Entering the radius of the disk
puts ("Please enter the radius: ");

//Reading the entered value
scanf ("%d", &radius);

//Calculating the area of the disk
area=Pi_Value*radius*radius;

//Printing the value of area
printf ("The area of the disk is: %f", area);

return 0;

}
